TEMPLATES=templates
SCRIPTS=scripts
CACHE=cache

PACKER_TEMPLATE=$(TEMPLATES)/packer-image.json
PACKER_VARIABLES=$(TEMPLATES)/packer-variables.json
# Packer Debug
#export PACKER_LOG=1

# Validate packer template
validate:
	$(SCRIPTS)/validate.sh $(PACKER_TEMPLATE) $(PACKER_VARIABLES)

# Build image
build: install_packer validate
	$(SCRIPTS)/build.sh $(PACKER_TEMPLATE) $(PACKER_VARIABLES)

# Install Packer binary
install_packer:
	$(SCRIPTS)/download_packer.sh
