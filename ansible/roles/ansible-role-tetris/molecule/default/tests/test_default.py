import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_httpd_is_installed(host):
    p = host.package("httpd")
    assert p.is_installed


def test_httpd_running_and_enabled(host):
    d = host.service("httpd")
    assert d.is_running
    assert d.is_enabled
