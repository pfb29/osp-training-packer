#!/bin/bash

VERSION=1.3.3
URL=https://releases.hashicorp.com/packer/${VERSION}/packer_${VERSION}_linux_amd64.zip
CACHE=${CACHE:-cache}
PACKER=$CACHE/packer.io

# Check cache directory
[ ! -d "$CACHE" ] && mkdir -p $CACHE

if [[ ! -x "$PACKER" ]]; then
    echo "Downloading packer"
    curl $URL -o - | gunzip - > $PACKER && chmod +x $PACKER
else
    echo "packer already present in $PACKER"
fi

echo "Check packer version $PACKER"
$PACKER version

exit
