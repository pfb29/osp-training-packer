#!/bin/bash  

set -e 

[ -z "$1" ] && echo "Error: \$PACKER_TEMPLATE parameter is not defined" >&2 && exit -1
[ ! -z $2 ] && PACKER_VARIABLES=$2

PACKER_TEMPLATE=$1
CACHE=${CACHE:-cache}
PACKER=$CACHE/packer.io

echo "Inspect packer template"
$PACKER inspect $PACKER_TEMPLATE
echo "Validate packer template ${PACKER_TEMPLATE}"
if [ -z $PACKER_VARIABLES ]; then
  $PACKER validate $PACKER_TEMPLATE
else
  $PACKER validate -var-file=$PACKER_VARIABLES $PACKER_TEMPLATE
fi

exit
