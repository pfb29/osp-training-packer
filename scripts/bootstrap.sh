#!/usr/bin/env bash

set -e 

# TODO: ansible role to tune guest images
# https://redhatstackblog.redhat.com/2017/01/18/9-tips-to-properly-configure-your-openstack-instance/
yum install -y tuned
systemctl enable tuned
tuned-adm profile virtual-guest

# Proceed with APT/YUM or tasks, Security Updates, etc

# Install ansible on remote host
curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
python get-pip.py
pip -V
pip install --quiet --upgrade ansible
pip install --quiet --upgrade jmespath

# Remove these options to speedup ssh connections configuring the instance
sed -i 's/#UseDNS yes/UseDNS no/g' /etc/ssh/sshd_config
sed -i 's/GSSAPIAuthentication yes/GSSAPIAuthentication no/g' /etc/ssh/sshd_config
service sshd reload

exit
