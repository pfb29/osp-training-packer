#!/bin/bash 

set -e 

[ -z "$1" ] && echo "Error: \$PACKER_TEMPLATE parameter is not defined" >&2 && exit -1
[ ! -z $2 ] && PACKER_VARIABLES=$2

PACKER_TEMPLATE=$1
CACHE=${CACHE:-cache}
PACKER=$CACHE/packer.io

mkdir -p $CACHE

PACKER_OUT=$CACHE/report
IMAGE_OUT=$CACHE/image

# Build the image
echo "Build instance with template ${PACKER_TEMPLATE} and Playbook ${PACKER_PLAYBOOK}"

if [ -z $PACKER_VARIABLES ]; then
  $PACKER build $PACKER_TEMPLATE | tee $PACKER_OUT
else
  # PACKER_LOG=1 $PACKER build -debug -var-file=$PACKER_VARIABLES $PACKER_TEMPLATE 
  $PACKER build -var-file=$PACKER_VARIABLES $PACKER_TEMPLATE | tee $PACKER_OUT
fi
# TODO: function to search errors in packer steps
if [ -z "`grep -i 'Non-zero exit status:' $PACKER_OUT 2>/dev/null`" ]; then
# if [ -z "`grep 'openstack' $PACKER_OUT | grep -i 'Error' 2>/dev/null`" ]; then
    grep "Waiting" $PACKER_OUT | sed -n 's/.*\(webserver-[^ ]*\).*/\1/p' > $IMAGE_OUT
    IMAGE=`cat $IMAGE_OUT`
    MESSAGE="New build base image `cat ${CACHE}/image`"
    JOB_ERR=0
else
    MESSAGE="ERROR! Something went wrong building this image: $PACKER_TEMPLATE $PACKER_VARIABLES ${PACKER_PLAYBOOK}"
    JOB_ERR=1
fi
echo $MESSAGE

# Return build status to pipeline
exit $JOB_ERR
